/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * LogUtil
 *
 * @since 2021-02-26
 */
public class LogUtil {
    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x01, "CalendarView");

    /**
     * From: width=%d, height=%f, text=%s
     * To: width=%{public}d, height=%{public}f, text=%{public}s
     * 不支持类似 %02d 这样的补位
     *
     * @param logMessageFormat String
     * @return log
     */
    public static String replaceFormat(String logMessageFormat) {
        return logMessageFormat.replaceAll("%([d|f|s])", "%{public}$1");
    }

    /**
     * debug
     *
     * @param tag String
     * @param format String
     * @param args Object
     */
    public static void debug(String tag, String format, Object... args) {
        HiLog.debug(label, tag + " " + replaceFormat(format), args);
    }

    /**
     * info
     *
     * @param info String
     */
    public static void info(String info) {
        HiLog.info(label, info);
    }

    /**
     * info
     *
     * @param tag String
     * @param format String
     * @param args Object
     */
    public static void info(String tag, String format, Object... args) {
        HiLog.info(label, tag + " " + replaceFormat(format), args);
    }

    /**
     * warn
     *
     * @param tag String
     * @param format String
     * @param args Object
     */
    public static void warn(String tag, String format, Object... args) {
        HiLog.warn(label, tag + " " + replaceFormat(format), args);
    }

    /**
     * error
     *
     * @param tag String
     * @param format String
     * @param args Object
     */
    public static void error(String tag, String format, Object... args) {
        HiLog.error(label, tag + " " + replaceFormat(format), args);
    }
}
