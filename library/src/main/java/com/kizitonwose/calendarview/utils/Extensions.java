/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Rect;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

/**
 * Extensions
 *
 * @since 2021-02-26
 */
public class Extensions {
    /**
     * NO_INDEX
     */
    public static final int NO_INDEX = -1;

    /**
     * inflate Component
     *
     * @param componentContainer ComponentContainer
     * @param layoutRes int
     * @param isAttachToRoot boolean
     * @return Component
     */
    public static final Component inflate(ComponentContainer componentContainer,
                                          int layoutRes, boolean isAttachToRoot) {
        Component component = LayoutScatter.getInstance(componentContainer.getContext()).parse(layoutRes,
            componentContainer, isAttachToRoot);
        return component;
    }

    /**
     * orFalse
     *
     * @param b Boolean
     * @return boolean
     */
    public static final boolean orFalse(Boolean b) {
        return b != null ? b : false;
    }

    /**
     * orZero
     *
     * @param zero Integer
     * @return zero
     */
    public static final int orZero(Integer zero) {
        return zero != null ? zero : 0;
    }

    /**
     * getYearMonth
     *
     * @param date LocalDate
     * @return YearMonth
     */
    public static final YearMonth getYearMonth(LocalDate date) {
        YearMonth yearMonth = YearMonth.of(date.getYear(), date.getMonth());
        return yearMonth;
    }

    /**
     * getNext YearMonth
     *
     * @param date LocalDate
     * @return YearMonth
     */
    public static final YearMonth getNext(LocalDate date) {
        YearMonth mYearMonth = getYearMonth(date).plusMonths(1L);
        return mYearMonth;
    }

    /**
     * getNext YearMonth
     *
     * @param yearMonth YearMonth
     * @return YearMonth
     */
    public static final YearMonth getNext(YearMonth yearMonth) {
        YearMonth mYearMonth = yearMonth.plusMonths(1L);
        return mYearMonth;
    }

    /**
     * getPrevious YearMonth
     *
     * @param date LocalDate
     * @return YearMonth
     */
    public static final YearMonth getPrevious(LocalDate date) {
        YearMonth yearMonth = getYearMonth(date).minusMonths(1L);
        return yearMonth;
    }

    /**
     * getNamedString
     *
     * @param rect Rect
     * @return name
     */
    public static final String getNamedString(Rect rect) {
        return "[L: " + rect.left + ", T: " + rect.top + "][R: " + rect.right + ", B: " + rect.bottom + ']';
    }

    /**
     * 分割List
     *
     * @param list 待分割的list
     * @param pageSize 每段list的大小
     * @param <T> 泛型T
     * @return List<< List < T>>
     */
    public static <T> List<List<T>> splitList(List<T> list, int pageSize) {
        List<List<T>> listArray = new ArrayList<List<T>>();
        List<T> subList = null;
        for (int i = 0; i < list.size(); i++) {
            if (i % pageSize == 0) { // 每次到达页大小的边界就重新申请一个subList
                subList = new ArrayList<T>();
                listArray.add(subList);
            }
            subList.add(list.get(i));
        }
        return listArray;
    }

    /**
     * getLast
     *
     * @param list List<T>
     * @param <T> 泛型T
     * @return list
     */
    public static <T> T getLast(List<T> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        int size = list.size();
        return list.get(size - 1);
    }

    /**
     * take
     *
     * @param list List<T>
     * @param num int
     * @param <T> 泛型T
     * @return List
     */
    public static <T> List<T> take(List<T> list, int num) {
        List<T> mList = null;
        int size = list.size();
        if (size <= num) {
            return list;
        }
        mList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            mList.add(list.get(i));
        }

        return mList;
    }
}
