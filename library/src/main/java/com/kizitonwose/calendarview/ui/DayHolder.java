/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.ui;

import com.kizitonwose.calendarview.bean.DayConfig;
import com.kizitonwose.calendarview.model.CalendarDay;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;

/**
 * DayHolder
 *
 * @since 2021-02-26
 */
public class DayHolder {
    private DayConfig config;
    private CalendarDay day;
    private ViewContainer viewContainer;
    private Component dateComponent;

    /**
     * DayHolder
     *
     * @param config DayConfig
     */
    public DayHolder(DayConfig config) {
        this.config = config;
    }

    /**
     * inflateDayView
     *
     * @param parent DirectionalLayout
     * @return Component
     */
    public Component inflateDayView(DirectionalLayout parent) {
        dateComponent = LayoutScatter.getInstance(parent.getContext()).parse(config.getDayViewRes(), null, false);

        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(dateComponent.getLayoutConfig());

        layoutConfig.width = config.getSize().getWidth() - layoutConfig.getMarginLeft() - layoutConfig.getMarginRight();
        layoutConfig.height = config.getSize().getHeight() - layoutConfig.getMarginTop() - layoutConfig.getMarginBottom();
        layoutConfig.weight = 1f;

        dateComponent.setLayoutConfig(layoutConfig);
        return dateComponent;
    }

    /**
     * bindDayView
     *
     * @param currentDay CalendarDay
     */
    public void bindDayView(CalendarDay currentDay) {
        this.day = currentDay;
        if (viewContainer == null) {
            viewContainer = config.getViewBinder().create(dateComponent);
        }

        int dayHash = 0;
        if (currentDay != null) {
            dayHash = currentDay.getDate().hashCode();
        }
        if (getComponentTag(viewContainer.getComponent()) != dayHash) {
            viewContainer.getComponent().setTag(dayHash);
        }

        if (currentDay != null) {
            if (viewContainer.getComponent().getVisibility() != Component.VISIBLE) {
                viewContainer.getComponent().setVisibility(Component.VISIBLE);
            }
            config.getViewBinder().bind(viewContainer, currentDay);
        } else if (viewContainer.getComponent().getVisibility() != Component.HIDE) {
            viewContainer.getComponent().setVisibility(Component.HIDE);
        }
    }

    /**
     * reloadViewIfNecessary
     *
     * @param day CalendarDay
     * @return reloadViewIfNecessary
     */
    public boolean reloadViewIfNecessary(CalendarDay day) {
        if (day == this.day) {
            bindDayView(this.day);
            return true;
        } else {
            return false;
        }
    }

    private int getComponentTag(Component component) {
        int tag = -1;
        Object object = component.getTag();
        if (object instanceof Integer) {
            tag = Integer.parseInt(object.toString());
        }
        return tag;
    }
}
