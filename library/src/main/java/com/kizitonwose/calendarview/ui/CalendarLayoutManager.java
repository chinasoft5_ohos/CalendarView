/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.ui;

import com.kizitonwose.calendarview.CalendarView;
import com.kizitonwose.calendarview.model.CalendarDay;
import com.kizitonwose.calendarview.utils.LogUtil;

import java.time.YearMonth;

/**
 * CalendarLayoutManager
 *
 * @since 2021-02-26
 */
public class CalendarLayoutManager {
    private static final String TAG = "CalendarLayoutManager";
    private CalendarView calView;


    public CalendarLayoutManager(CalendarView calView) {
        this.calView = calView;
    }

    /**
     * 滑动到指定位置
     *
     * @param month YearMonth
     */
    public void scrollToMonth(YearMonth month) {
        scrollToPosition(getItemProvider().getAdapterPosition(month), false);
    }

    /**
     * 滑动到指定位置
     *
     * @param month YearMonth
     */
    public void smoothScrollToMonth(YearMonth month) {
        scrollToPosition(getItemProvider().getAdapterPosition(month), true);
    }

    /**
     * 滑动到指定位置
     *
     * @param day CalendarDay
     */
    public void scrollToDay(CalendarDay day) {
        scrollToPosition(getItemProvider().getAdapterPosition(day), false);
    }

    /**
     * 滑动到指定位置
     *
     * @param day CalendarDay
     */
    public void smoothScrollToDay(CalendarDay day) {
        scrollToPosition(getItemProvider().getAdapterPosition(day), true);
    }

    /**
     * 滑动到指定位置
     *
     * @param position int
     * @param isSmoothScroll boolean
     */
    public synchronized void scrollToPosition(int position, boolean isSmoothScroll) {
        System.out.println(TAG + "position == " + position);
        if (position < 0) {
            calView.scrollTo(0, 0);
            return;
        }
        calView.getContext().getUITaskDispatcher().asyncDispatchBarrier(() -> {
            if (position == 0) {
                calView.scrollTo(0, 0);
            } else {
                calView.scrollTo(position);
            }

            LogUtil.info(TAG, "我滑动到了月份：" + position);
        });
        calView.getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                getItemProvider().notifyMonthScrollListenerIfNeeded();
            }
        });
    }

    /**
     * 获取Item提供者
     *
     * @return CalendarAdapter
     */
    public CalendarAdapter getItemProvider() {
        return (CalendarAdapter) calView.getItemProvider();
    }
}
