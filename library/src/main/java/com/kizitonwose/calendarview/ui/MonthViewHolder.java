/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.ui;

import com.kizitonwose.calendarview.model.CalendarDay;
import com.kizitonwose.calendarview.model.CalendarMonth;
import com.kizitonwose.calendarview.uinterface.MonthHeaderFooterBinder;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.List;

/**
 * MonthViewHolder
 *
 * @since 2021-02-26
 */
public class MonthViewHolder {
    private CalendarAdapter adapter;
    private ComponentContainer rootLayout;
    private List<WeekHolder> weekHolders;
    private MonthHeaderFooterBinder<ViewContainer> monthHeaderBinder;
    private MonthHeaderFooterBinder<ViewContainer> monthFooterBinder;
    private ViewContainer headerContainer;
    private ViewContainer footerContainer;
    private CalendarMonth month;
    private Component headerComponent;
    private Component footerComponent;

    /**
     * MonthViewHolder
     *
     * @param adapter CalendarAdapter
     * @param rootLayout ComponentContainer
     * @param weekHolders List
     * @param monthHeaderBinder MonthHeaderFooterBinder
     * @param monthFooterBinder MonthHeaderFooterBinder
     */
    public MonthViewHolder(CalendarAdapter adapter,
                           ComponentContainer rootLayout,
                           List<WeekHolder> weekHolders,
                           MonthHeaderFooterBinder<ViewContainer> monthHeaderBinder,
                           MonthHeaderFooterBinder<ViewContainer> monthFooterBinder) {
        this.adapter = adapter;
        this.rootLayout = rootLayout;
        this.weekHolders = weekHolders;
        this.monthHeaderBinder = monthHeaderBinder;
        this.monthFooterBinder = monthFooterBinder;

        headerComponent = rootLayout.findComponentById(adapter.headerViewId);
        footerComponent = rootLayout.findComponentById(adapter.footerViewId);
    }

    /**
     * bindMonth
     *
     * @param month CalendarMonth
     */
    public void bindMonth(CalendarMonth month) {
        this.month = month;

        if (headerComponent != null) {
            if (headerContainer == null && monthHeaderBinder != null) {
                headerContainer = monthHeaderBinder.create(headerComponent);
            }
            if (monthHeaderBinder != null) {
                monthHeaderBinder.bind(headerContainer, month);
            }
        }

        if (footerComponent != null) {
            if (footerContainer == null && monthFooterBinder != null) {
                footerContainer = monthFooterBinder.create(footerComponent);
            }
            if (monthFooterBinder != null) {
                monthFooterBinder.bind(footerContainer, month);
            }
        }

        List<List<CalendarDay>> weekDays = month.getWeekDays();
        for (int index = 0; index < weekHolders.size(); index++) {
            WeekHolder weekHolder = weekHolders.get(index);
            if (index < weekDays.size()) {
                weekHolder.bindWeekView(weekDays.get(index));
            } else {
                weekHolder.bindWeekView(null);
            }
        }
    }

    /**
     * reloadDay
     *
     * @param day CalendarDay
     */
    public void reloadDay(CalendarDay day) {
        for (WeekHolder weekHolder : weekHolders) {
            weekHolder.reloadDay(day);
        }
    }
}
