/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.uinterface;

import com.kizitonwose.calendarview.model.CalendarMonth;

/**
 * 将kotlin中的类型别名转成用Java接口实现
 *
 * 对应的调用处需改用Java接口的方式使用
 *
 * @since 2021-02-26
 */
public interface MonthScrollListener {
    /**
     * onMonthScroll
     *
     * @param calendarMonth CalendarMonth
     */
    void onMonthScroll(CalendarMonth calendarMonth);
}
