/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.bean;

import com.kizitonwose.calendarview.ui.ViewContainer;
import com.kizitonwose.calendarview.uinterface.DayBinder;
import com.kizitonwose.calendarview.utils.Size;

/**
 * DayConfig
 *
 * @since 2021-02-26
 */
public class DayConfig {
    private Size size;
    private int dayViewRes;
    private DayBinder<ViewContainer> viewBinder;

    /**
     * DayConfig
     *
     * @param size Size
     * @param dayViewRes int
     * @param viewBinder DayBinder<>
     */
    public DayConfig(Size size, int dayViewRes, DayBinder<ViewContainer> viewBinder) {
        this.size = size;
        this.dayViewRes = dayViewRes;
        this.viewBinder = viewBinder;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Size getSize() {
        return size;
    }

    public void setDayViewRes(int dayViewRes) {
        this.dayViewRes = dayViewRes;
    }

    public int getDayViewRes() {
        return dayViewRes;
    }

    public void setViewBinder(DayBinder<ViewContainer> viewBinder) {
        this.viewBinder = viewBinder;
    }

    public DayBinder<ViewContainer> getViewBinder() {
        return viewBinder;
    }
}
