/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.bean;

/**
 * ViewConfig
 *
 * @since 2021-02-26
 */
public class ViewConfig {
    /**
     * dayViewRes
     */
    private int dayViewRes;

    /**
     * monthHeaderRes
     */
    private int monthHeaderRes;

    /**
     * monthFooterRes
     */
    private int monthFooterRes;

    /**
     * monthViewClass
     */
    private String monthViewClass;

    /**
     * ViewConfig
     *
     * @param dayViewRes 天数视图ID
     * @param monthHeaderRes 月份头视图ID
     * @param monthFooterRes 月份脚视图ID
     * @param monthViewClass monthView的类名
     */
    public ViewConfig(int dayViewRes, int monthHeaderRes, int monthFooterRes, String monthViewClass) {
        this.dayViewRes = dayViewRes;
        this.monthFooterRes = monthFooterRes;
        this.monthHeaderRes = monthHeaderRes;
        this.monthViewClass = monthViewClass;
    }

    public void setDayViewRes(int dayViewRes) {
        this.dayViewRes = dayViewRes;
    }

    public void setMonthHeaderRes(int monthHeaderRes) {
        this.monthHeaderRes = monthHeaderRes;
    }

    public void setMonthFooterRes(int monthFooterRes) {
        this.monthFooterRes = monthFooterRes;
    }

    public void setMonthViewClass(String monthViewClass) {
        this.monthViewClass = monthViewClass;
    }

    public String getMonthViewClass() {
        return monthViewClass;
    }

    public int getDayViewRes() {
        return dayViewRes;
    }

    public int getMonthHeaderRes() {
        return monthHeaderRes;
    }

    public int getMonthFooterRes() {
        return monthFooterRes;
    }
}
