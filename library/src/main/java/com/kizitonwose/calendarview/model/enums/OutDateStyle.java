/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.model.enums;

/**
 * OutDateStyle
 *
 * @since 2021-02-26
 */
public enum OutDateStyle {
    /**
     * The calendar will generate outDates until it reaches
     * the first end of (a) row. This means that if  (a) month
     * has 6 rows, it will display 6 rows and if (a) month
     * has 5 rows, it will display 5 rows.
     */
    END_OF_ROW,

    /**
     * The calendar will generate outDates until
     * it reaches the end of (a) 6 (x) 7 grid.
     * This means that all months will have 6 rows.
     */
    END_OF_GRID,

    /**
     * outDates will not be generated.
     */
    NONE
}
