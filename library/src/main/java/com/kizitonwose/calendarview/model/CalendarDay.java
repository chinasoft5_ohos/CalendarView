/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.model;

import com.kizitonwose.calendarview.model.enums.DayOwner;
import com.kizitonwose.calendarview.utils.Extensions;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.YearMonth;

/**
 * CalendarDay
 *
 * @since 2021-02-26
 */
public class CalendarDay implements Serializable {
    /**
     * YearMonth positionYearMonth
     */
    protected YearMonth positionYearMonth;
    private LocalDate date;
    private DayOwner owner;

    private int day;

    /**
     * CalendarDay
     *
     * @param date LocalDate
     * @param owner DayOwner
     */
    public CalendarDay(LocalDate date, DayOwner owner) {
        this.date = date;
        this.owner = owner;
        if (date != null) {
            day = date.getDayOfMonth();
        }
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DayOwner getOwner() {
        return owner;
    }

    public void setOwner(DayOwner owner) {
        this.owner = owner;
    }

    /**
     * getPositionYearMonth
     *
     * @return YearMonth
     */
    public YearMonth getPositionYearMonth() {
        try {
            if (owner == DayOwner.THIS_MONTH) {
                positionYearMonth = Extensions.getYearMonth(date);
            } else if (owner == DayOwner.PREVIOUS_MONTH) {
                positionYearMonth = Extensions.getNext(date);
            } else if (owner == DayOwner.NEXT_MONTH) {
                positionYearMonth = Extensions.getPrevious(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return positionYearMonth;
    }

    @Override
    public String toString() {
        return "CalendarDay { date =  " + date + ", owner = " + owner + "}";
    }

    @Override
    public int hashCode() {
        return 31 * (this.date.hashCode() + this.owner.hashCode());
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (!(other instanceof CalendarDay)) {
            return false;
        } else {
            CalendarDay calendarDay = (CalendarDay) other;
            return date.equals(calendarDay.getDate()) && owner == calendarDay.getOwner();
        }
    }

    public int getDay() {
        return day;
    }
}
