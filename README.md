# CalendarView


#### 项目介绍
- 项目名称：CalendarView
- 所属系列：openharmony的第三方组件适配移植
- 功能：自定义各种风格日历
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：release v1.0.4


#### 效果演示

![art1](images/fraction1_7.gif)



#### 安装教程
在moudle级别下的build.gradle文件中添加依赖，在dependencies标签中增加对libs目录下jar包的引用。

```groovy
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
    }
}

// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:CalendarView:0.0.1-SNAPSHOT'
}

 ```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

##### 步骤1:

添加CalendarView到你的xml布局文件

```xml
<com.kizitonwose.calendarview.CalendarView
    ohos:id="@+id/calendarView"
    ohos:layout_width="match_parent"
    ohos:layout_height="wrap_content"
    app:cv_dayViewResource="@layout/calendar_day_layout" />
```

创建 日 的xml布局在 `res/layout/calendar_day_layout.xml`;

```xml
<TextView
    ohos:id="@+id/calendarDayText"
    ohos:layout_width="match_parent"
    ohos:layout_height="match_parent"
    ohos:gravity="center"
    ohos:textSize="16sp"
    tools:text="22" />
```

创建视图容器,作为每个日期单元格的视图容器;

```java
class DayViewContainer(view: View) : ViewContainer(view) {    
    Text text = (Text) component.findComponentById(ResourceTable.Id_calendarDayText);
}
```

为CalendarView提供"DayViewContainer"类型的"DayBinder";

````java
calendarView.setDayBinder(new DayBinder<DayViewContainer>() {
            @Override
            public DayViewContainer create(Component component) {
                return new DayViewContainer(component);
            }

            @Override
            public void bind(DayViewContainer container, CalendarDay day) {
                container.bind(day);
            }
        });
````

##### 步骤 2:

````java
        YearMonth currentMonth = YearMonth.now();
        calendarView.setup(currentMonth, currentMonth.plusMonths(3), LocalDate.now().getDayOfWeek());
        calendarView.scrollToDate(LocalDate.now());
````

#### 相关属性

- **cv_dayViewResource**: 用作日单元格视图,必须提供;

- **cv_monthHeaderResource**: 用作每个月的标题;

- **cv_monthFooterResource**: 用作每个月的页脚;

- **cv_orientation**: CalendarView布局的方向，可以选择 cv_orientation_horizontal 和 cv_orientation_vertical;



#### 测试信息

CodeCheck代码测试无异常
  
CloudTest代码测试无异常 
 
病毒安全检测通过 
 
当前版本demo功能与原组件基本无差异 
 
#### 版本迭代
- 0.0.1-SNAPSHOT  

#### 版权和许可信息
CalendarView is distributed under the MIT license. See LICENSE for details.

