/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview;

/**
 * NumberRange
 *
 * @version 1.0
 * @date 2021/2/22/022 15:00
 */
public class NumberRange {

    private Number start;

    private Number endInclusive;

    /**
     * 构造函数
     *
     * @param start 开始的数
     * @param endInclusive 结束的数字
     */
    public NumberRange(Number start, Number endInclusive) {
        this.start = start;
        this.endInclusive = endInclusive;
    }

    public Number getStart() {
        return start;
    }

    public Number getEndInclusive() {
        return endInclusive;
    }
}
