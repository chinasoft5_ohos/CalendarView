/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview;

import com.kizitonwose.calendarview.fraction.BaseFraction;
import com.kizitonwose.calendarview.slice.MainAbilitySlice;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.multimodalinput.event.KeyEvent;

/**
 * MainAbility
 *
 * @version 1.0
 * @since 2021-02-26
 */
public class MainAbility extends FractionAbility {
    private BaseFraction currentFaction;
    private MainAbilitySlice currentAbilitySlice;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent keyEvent) {
        if (currentFaction != null && keyCode == KeyEvent.KEY_BACK) {
            // 执行的是弹出栈操作，弹出后需重新入栈（对应add方法）
            getFractionManager().popFromStack();
            currentFaction = null;
            currentAbilitySlice.setCurrentFaction(null);
            return true;
        }
        return super.onKeyUp(keyCode, keyEvent);
    }

    public void setCurrentFaction(BaseFraction currentFaction) {
        this.currentFaction = currentFaction;
    }

    public void setCurrentAbilitySlice(MainAbilitySlice currentAbilitySlice) {
        this.currentAbilitySlice = currentAbilitySlice;
    }

    public MainAbilitySlice getCurrentAbilitySlice() {
        return currentAbilitySlice;
    }
}
