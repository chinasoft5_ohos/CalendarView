/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview;

import com.kizitonwose.calendarview.fraction.BaseFraction;

/**
 * ExampleItem
 *
 * @version 1.0
 * @date 2021/2/18/ 15:03
 */
public class ExampleItem {

    private int titleRes;
    private int subtitleRes;
    private Function<BaseFraction> createView;

    public ExampleItem(int titleRes, int subtitleRes, Function<BaseFraction> createView) {
        this.titleRes = titleRes;
        this.subtitleRes = subtitleRes;
        this.createView = createView;
    }

    public int getTitleRes() {
        return titleRes;
    }

    public void setTitleRes(int titleRes) {
        this.titleRes = titleRes;
    }

    public int getSubtitleRes() {
        return subtitleRes;
    }

    public void setSubtitleRes(int subtitleRes) {
        this.subtitleRes = subtitleRes;
    }

    public BaseFraction getCreateView() {
        return createView.invoke();
    }

}
