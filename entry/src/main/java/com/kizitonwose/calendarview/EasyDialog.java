/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;

/**
 * EasyDialog
 *
 * @version 1.0
 * @date 2021/2/26 9:43
 */
public class EasyDialog {
    private CommonDialog dialog;
    private Context context;
    private Component parse;

    private EasyDialog(Context context) {
        this.context = context;
        this.dialog = new CommonDialog(context);
    }

    /**
     * EasyDialog 获取EasyDialog对象
     *
     * @param context
     * @return EasyDialog
     */
    public static EasyDialog getInstance(Context context) {
        return new EasyDialog(context);
    }

    class MyCommonDialog extends CommonDialog {

        public MyCommonDialog(Context context) {
            super(context);
        }

        @Override
        protected void onCreate() {
            super.onCreate();
        }
    }

    /**
     * 设置内容ID
     *
     * @param resId
     * @return EasyDialog
     */
    public EasyDialog setContentResId(int resId) {
        parse = LayoutScatter.getInstance(context).parse(resId, null, false);
        dialog.setContentCustomComponent(parse);
        return this;
    }

    /**
     * 设置text
     *
     * @param id
     * @param str
     * @return EasyDialog
     */
    public EasyDialog setText(int id, String str) {
        ((Text) findComponentById(id)).setText(str);
        return this;
    }

    /**
     * 获取Text
     *
     * @param id
     * @return Text
     */
    public Text getText(int id) {
        return findComponentById(id);
    }

    /**
     * 根据id获取TextField
     *
     * @param id
     * @return TextField
     */
    public TextField getTextField(int id) {
        return findComponentById(id);
    }

    /**
     * 设置监听
     *
     * @param id
     * @param clickedListener
     * @return EasyDialog
     */
    public EasyDialog setClickListener(int id, ClickedListener clickedListener) {
        findComponentById(id).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                clickedListener.onClick(EasyDialog.this);
            }
        });
        return this;
    }

    /**
     * 展示EasyDialog
     *
     * @return EasyDialog
     */
    public EasyDialog show() {
        dialog.show();
        return this;
    }

    /**
     * 设置title文本
     *
     * @param text
     * @return EasyDialog
     */
    public EasyDialog setTitleText(String text) {
        dialog.setTitleText(text);
        return this;
    }

    /**
     * 设置EasyDialog大小
     *
     * @param width
     * @param height
     * @return EasyDialog
     */
    public EasyDialog setSize(int width, int height) {
        dialog.setSize(width, height);
        return this;
    }

    /**
     * 关闭EasyDialog
     *
     * @return EasyDialog
     */
    public EasyDialog destroy() {
        dialog.destroy();
        return this;
    }

    /**
     * 注册显示监听
     *
     * @param listener
     * @return EasyDialog
     */
    public EasyDialog registerDisplayListener(DisplayListener listener) {
        dialog.registerDisplayCallback(new BaseDialog.DisplayCallback() {
            @Override
            public void onDisplay(IDialog iDialog) {
                listener.onDisplay(EasyDialog.this);
            }
        });
        return this;
    }


    private <T extends Component> T findComponentById(int id) {
        return (T) parse.findComponentById(id);
    }

    public CommonDialog getDialog() {
        return dialog;
    }

    /**
     * 接口ClickedListener
     */
    public interface ClickedListener {
        void onClick(EasyDialog dialog);
    }

    /**
     * 接口DisplayListener
     */
    public interface DisplayListener {
        void onDisplay(EasyDialog dialog);
    }
}
