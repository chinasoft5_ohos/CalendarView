/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview;

import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.List;

/**
 * HomeOptionsProvider
 *
 * @version 1.0
 * @date 2021/2/18 16:21
 */
public class HomeOptionsProvider extends QuickBaseItemProvider<ExampleItem> {

    public HomeOptionsProvider(Context context,List<ExampleItem> mData) {
        super(context,mData);
    }

    @Override
    protected int getComponentResourceLayout(ComponentContainer componentContainer, int componentType) {
        return ResourceTable.Layout_home_options_item_view;
    }

    @Override
    protected void bindComponentHolder(BaseComponentHolder holder, int position, int componentType) {
        try {
            holder.setText(ResourceTable.Id_itemOptionTitle, getContext().getResourceManager().getElement(getItem(position).getTitleRes()).getString());
            holder.setText(ResourceTable.Id_itemOptionSubtitle, getContext().getResourceManager().getElement(getItem(position).getSubtitleRes()).getString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

}
