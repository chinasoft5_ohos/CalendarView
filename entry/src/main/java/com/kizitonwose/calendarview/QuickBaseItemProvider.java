/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * QuickBaseItemProvider
 *
 * @version 1.0
 * @date 2021/2/18 19:00
 */
public abstract class QuickBaseItemProvider<E> extends BaseItemProvider {
    private List<E> list;
    private Context context;

    public QuickBaseItemProvider(Context context) {
        this(context, new ArrayList<>());
    }

    public QuickBaseItemProvider(Context context, List<E> list) {
        this.list = list;
        this.context = context.getApplicationContext();
    }

    public Context getContext() {
        return context;
    }

    public List<E> getList() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public E getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        BaseComponentHolder holder;
        if (component == null) {
            holder = createComponentHolder(componentContainer, getItemComponentType(position));
            holder.getComponentById().setTag(holder);
        } else {
            holder = (BaseComponentHolder) component.getTag();
        }
        bindComponentHolder(holder, position, getItemComponentType(position));
        return holder.getComponentById();
    }

    private BaseComponentHolder createComponentHolder(ComponentContainer componentContainer, int componentType) {
        return new BaseComponentHolder(context, getComponentResourceLayout(componentContainer, componentType)) {
        };
    }

    /**
     * getComponentResourceLayout 获取内容布局
     *
     * @param componentContainer ComponentContainer
     * @param componentType component类型
     * @return int 资源
     */
    protected abstract int getComponentResourceLayout(ComponentContainer componentContainer, int componentType);

    /**
     * bindComponentHolder
     *
     * @param holder BaseComponentHolder
     * @param position 角标
     * @param componentType component类型
     */
    protected abstract void bindComponentHolder(BaseComponentHolder holder, int position, int componentType);

    /**
     * BaseComponentHolder
     *
     * @version 1.0
     * @date 2021/2/4/004 19:00
     */
    public abstract static class BaseComponentHolder {
        protected Component component;
        protected Map<Integer, Component> views = new HashMap<>();

        /**
         * BaseComponentHolder
         *
         * @param context 上下文
         * @param layoutResId 资源ID
         */
        public BaseComponentHolder(Context context, int layoutResId) {
            this.component = LayoutScatter.getInstance(context).parse(layoutResId, null, false);
        }

        /**
         * 获取Component
         *
         * @return Component
         */
        public Component getComponentById() {
            return component;
        }

        /**
         * BaseComponentHolder
         *
         * @param id 资源ID
         * @param str 文本
         * @return BaseComponentHolder
         */
        public BaseComponentHolder setText(int id, String str) {
            Text text = getComponentById(id);
            text.setText(str);
            return this;
        }

        /**
         * setPixelMap设置PixelMap
         *
         * @param id 资源ID
         * @param pixelMap 像素图
         * @return BaseComponentHolder
         */
        public BaseComponentHolder setPixelMap(int id, PixelMap pixelMap) {
            Image image = getComponentById(id);
            image.setPixelMap(pixelMap);
            return this;
        }

        /**
         * setBackground 设置背景
         *
         * @param id 资源ID
         * @param element 元素
         * @return BaseComponentHolder
         */
        public BaseComponentHolder setBackground(int id, Element element) {
            getComponentById(id).setBackground(element);
            return this;
        }


        /**
         * 获取控件id
         *
         * @param id 资源ID
         * @param <T> 泛型T extends Component
         * @return 泛型T extends Component
         */
        public <T extends Component> T getComponentById(int id) {
            T t;
            if (!views.containsKey(id)) {
                t = findComponentById(id);
                views.put(id, t);
            } else {
                t = (T) views.get(id);
            }
            return t;
        }

        /**
         * 获取控件
         *
         * @param id 资源ID
         * @param <T> 泛型T extends Component
         * @return 泛型T extends Component
         */
        private <T extends Component> T findComponentById(int id) {
            return (T) component.findComponentById(id);
        }
    }
}
