/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.view;

import com.kizitonwose.calendarview.ResourceTable;
import com.kizitonwose.calendarview.ui.ViewContainer;
import ohos.agp.components.*;

/**
 * MonthView6Container
 *
 * @version 1.0
 * @since 2021-02-26
 */
public class MonthView6Container extends ViewContainer {

    private Text exSixMonthText;
    private DirectionalLayout legendLayout;

    public MonthView6Container(Component component) {
        super(component);
        exSixMonthText = (Text) component.findComponentById(ResourceTable.Id_exSixMonthText);
        legendLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_legendLayout);
    }

    public Text getExSixMonthText() {
        return exSixMonthText;
    }

    public DirectionalLayout getLegendLayout() {
        return legendLayout;
    }

}
