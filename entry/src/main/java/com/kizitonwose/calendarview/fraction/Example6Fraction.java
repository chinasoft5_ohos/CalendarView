/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.fraction;

import com.kizitonwose.calendarview.CalendarView;
import com.kizitonwose.calendarview.ResourceTable;
import com.kizitonwose.calendarview.model.CalendarDay;
import com.kizitonwose.calendarview.model.CalendarMonth;
import com.kizitonwose.calendarview.model.enums.DayOwner;
import com.kizitonwose.calendarview.uinterface.DayBinder;
import com.kizitonwose.calendarview.uinterface.MonthHeaderFooterBinder;
import com.kizitonwose.calendarview.utils.DayExtensions;
import com.kizitonwose.calendarview.utils.Size;
import com.kizitonwose.calendarview.view.DayView6Container;
import com.kizitonwose.calendarview.view.MonthView6Container;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

import java.time.DayOfWeek;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Example6Fraction
 *
 * @version 1.0
 * @since 2021-02-26
 */
public class Example6Fraction extends BaseFraction {

    private CalendarView calendarView;
    private DateTimeFormatter titleFormatter = DateTimeFormatter.ofPattern("MMM yyyy");

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_example_6_fraction;
    }

    @Override
    protected void initComponent(Component content, ComponentContainer container, Intent intent) {
        calendarView = findComponentById(ResourceTable.Id_cv_example_6);
        Image imageBack = findComponentById(ResourceTable.Id_image_back);
        imageBack.setClickedListener(component -> {
            backFraction();
        });
        initCalendarView();
    }

    private void initCalendarView() {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(mContext);
        int monthWidth = (int) (display.get().getAttributes().width * 0.73);
        int dayWidth = monthWidth / 7;
        int dayHeight = (int) (dayWidth * 1.73);
        Size daySize = new Size(dayWidth, dayHeight);
        calendarView.setDaySize(daySize);
        int horizontalMargin = AttrHelper.vp2px(8, mContext);
        int verticalMargin = AttrHelper.vp2px(14, mContext);

        calendarView.setMonthMargins(horizontalMargin, horizontalMargin, verticalMargin, verticalMargin);

        calendarView.setDayBinder(new DayBinder<DayView6Container>() {
            @Override
            public DayView6Container create(Component component) {
                return new DayView6Container(component);
            }

            @Override
            public void bind(DayView6Container container, CalendarDay day) {
                try {
                    Text text = container.getText();
                    if (day.getOwner() == DayOwner.THIS_MONTH) {
                        text.setText(day.getDate().getDayOfMonth() + "");
                        text.setTextSize(14, Text.TextSizeType.FP);
                        text.setTextColor(new Color(mContext.getResourceManager().getElement(ResourceTable.Color_example_6_day_text_black).getColor()));
                        text.setVisibility(Component.VISIBLE);
                    } else {
                        text.setVisibility(Component.INVISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        DayOfWeek[] dayOfWeekList = DayExtensions.daysOfWeekFromLocale();
        YearMonth currentMonth = YearMonth.now();
        calendarView.setup(currentMonth.minusMonths(10), currentMonth.plusMonths(10), dayOfWeekList[0]);
        calendarView.scrollToMonth(currentMonth);
        calendarView.setMonthHeaderBinder(new MonthHeaderFooterBinder<MonthView6Container>() {

            @Override
            public MonthView6Container create(Component component) {
                return new MonthView6Container(component);
            }

            @Override
            public void bind(MonthView6Container viewContainer, CalendarMonth month) {
                viewContainer.getExSixMonthText().setText(titleFormatter.format(month.getYearMonth()));
                if (viewContainer.getLegendLayout().getTag() == null) {
                    viewContainer.getLegendLayout().setTag(month.getYearMonth());
                    DirectionalLayout directionalLayout = viewContainer.getLegendLayout();
                    int size = directionalLayout.getChildCount();
                    for (int i = 0; i < size; i++) {
                        Text text = (Text) directionalLayout.getComponentAt(i);
                        text.setText(String.valueOf(dayOfWeekList[i].name().charAt(0)));
                        text.setTextSize(14, Text.TextSizeType.FP);
                        try {
                            text.setTextColor(new Color(mContext.getResourceManager().getElement(ResourceTable.Color_example_6_day_text_black).getColor()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }


}
