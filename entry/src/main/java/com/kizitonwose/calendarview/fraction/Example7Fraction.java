/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.fraction;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;

import com.kizitonwose.calendarview.CalendarView;
import com.kizitonwose.calendarview.ResourceTable;
import com.kizitonwose.calendarview.model.CalendarDay;
import com.kizitonwose.calendarview.ui.ViewContainer;
import com.kizitonwose.calendarview.uinterface.DayBinder;
import com.kizitonwose.calendarview.utils.Size;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

/**
 * Example7Fraction
 *
 * @version 1.0
 * @since 2021-02-26
 */
public class Example7Fraction extends BaseFraction {

    private CalendarView calendarView;
    private Image backImage;
    private LocalDate selectedDate = LocalDate.now();
    private static final String TAG = "Example7Fraction";


    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_example_7_fraction;
    }

    @Override
    protected void initComponent(Component content, ComponentContainer container, Intent intent) {

        calendarView = findComponentById(ResourceTable.Id_exSevenCalendar);
        backImage = findComponentById(ResourceTable.Id_e7f_image_back);
        DisplayAttributes displayAttributes = DisplayManager.getInstance().getDefaultDisplay(mContext).get().getAttributes();
        int dayWidth = displayAttributes.width / 7;
        int dayHeight = (int) (dayWidth * 1.8);
        Size daySize = new Size(dayWidth, dayHeight);
        calendarView.setDaySize(daySize);

        calendarView.setDayBinder(new DayBinder<DayViewContainer>() {
            @Override
            public DayViewContainer create(Component component) {
                return new DayViewContainer(component);
            }

            @Override
            public void bind(DayViewContainer container, CalendarDay day) {
                container.bind(day);
            }
        });

        YearMonth currentMonth = YearMonth.now();
        calendarView.setup(currentMonth, currentMonth.plusMonths(3), LocalDate.now().getDayOfWeek());
        calendarView.scrollToDate(LocalDate.now());

        backImage.setClickedListener(component -> {
            backFraction();
        });
    }

    class DayViewContainer extends ViewContainer {

        private CalendarDay day;
        private Text exSevenMonthText;
        private Text exSevenDateText;
        private Text exSevenDayText;
        private Component exSevenSelectedView;
        private DateTimeFormatter dateFormatter;
        private DateTimeFormatter dayFormatter;
        private DateTimeFormatter monthFormatter;

        public DayViewContainer(Component component) {
            super(component);
            initData();
            initView(component);
            initListener(component);
        }

        private void initData() {
            dateFormatter = DateTimeFormatter.ofPattern("dd");
            dayFormatter = DateTimeFormatter.ofPattern("EEE");
            monthFormatter = DateTimeFormatter.ofPattern("MMM");
        }

        private void initView(Component component) {
            exSevenMonthText = (Text) component.findComponentById(ResourceTable.Id_exSevenMonthText);
            exSevenDateText = (Text) component.findComponentById(ResourceTable.Id_exSevenDateText);
            exSevenDayText = (Text) component.findComponentById(ResourceTable.Id_exSevenDayText);
            exSevenSelectedView = component.findComponentById(ResourceTable.Id_exSevenSelectedView);

        }

        private void initListener(Component component) {
            component.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    CalendarDay firstVisibleDay = calendarView.findFirstVisibleDay();
                    CalendarDay lastVisibleDay = calendarView.findLastVisibleDay();
                    if(firstVisibleDay != null  && firstVisibleDay.equals(day)){
                        calendarView.smoothScrollToDate(day.getDate());
                    }else if(lastVisibleDay != null && lastVisibleDay.equals(day)){
                        calendarView.smoothScrollToDate(day.getDate());
                    }

                    if(selectedDate != day.getDate()){
                        LocalDate oldDate = selectedDate;
                        selectedDate = day.getDate();
                        calendarView.notifyDateChanged(day.getDate());
                        if(oldDate != null){
                            calendarView.notifyDateChanged(oldDate);
                        }
                    }
                }
            });
        }

        /**
         * 绑定数据
         *
         * @param day
         */
        public void bind(CalendarDay day){
            this.day = day;
            exSevenDateText.setText(dateFormatter.format(day.getDate()));
            exSevenDayText.setText(dayFormatter.format(day.getDate()));
            exSevenMonthText.setText(monthFormatter.format(day.getDate()));
            try {
                if(day.getDate().equals(selectedDate)){

                    exSevenDateText.setTextColor(new Color(mContext.getResourceManager().getElement(ResourceTable.Color_example_7_yellow).getColor()));
                }else{
                    exSevenDateText.setTextColor(new Color(mContext.getResourceManager().getElement(ResourceTable.Color_example_7_white).getColor()));
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            exSevenSelectedView.setVisibility((day.getDate().equals(selectedDate)?Component.VISIBLE:Component.HIDE));

        }
    }

}
