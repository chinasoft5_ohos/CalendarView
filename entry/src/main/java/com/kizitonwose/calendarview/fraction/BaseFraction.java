/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.fraction;

import com.kizitonwose.calendarview.MainAbility;
import com.kizitonwose.calendarview.ResourceTable;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

/**
 * BaseFraction
 *
 * @version 1.0
 * @since 2021-02-26
 */
public abstract class BaseFraction extends Fraction {
    Context mContext;
    private Component content;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mContext = getFractionAbility().getContext();
        content = scatter.parse(getLayoutResId(), null, false);
        initComponent(content, container, intent);

        return content;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(getStatusBarColor());
        getFractionAbility().findComponentById(ResourceTable.Id_toolBar).setVisibility(Component.HIDE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getFractionAbility().findComponentById(ResourceTable.Id_toolBar).setVisibility(Component.VISIBLE);
        WindowManager.getInstance().getTopWindow().get()
            .setStatusBarColor(mContext.getColor(ResourceTable.Color_colorPrimaryDark));
    }

    /**
     * getLayoutResId
     *
     * @return ResId
     */
    protected abstract int getLayoutResId();

    /**
     * 获取StatusBar颜色值
     *
     * @return int 颜色值
     */
    protected int getStatusBarColor() {
        return mContext.getColor(ResourceTable.Color_colorPrimaryDark);
    }

    /**
     * initComponent
     *
     * @param content Component
     * @param container ComponentContainer
     * @param intent Intent
     */
    protected abstract void initComponent(Component content, ComponentContainer container, Intent intent);

    /**
     * 获取控件
     *
     * @param id 资源ID
     * @param <T> 泛型T继承Component
     * @return T extends Component
     */
    <T extends Component> T findComponentById(int id) {
        return (T) content.findComponentById(id);
    }

    /**
     * 退出当前Fraction
     */
    protected void backFraction() {
        ((MainAbility) getFractionAbility()).getCurrentAbilitySlice().setCurrentFaction(null);
        getFractionAbility().getFractionManager().popFromStack();
    }
}
