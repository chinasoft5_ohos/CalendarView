/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.slice;

import com.kizitonwose.calendarview.*;
import com.kizitonwose.calendarview.fraction.BaseFraction;
import com.kizitonwose.calendarview.fraction.Example1Fraction;
import com.kizitonwose.calendarview.fraction.Example6Fraction;
import com.kizitonwose.calendarview.fraction.Example7Fraction;
import com.kizitonwose.calendarview.fraction.*;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.window.service.WindowManager;

import java.util.ArrayList;
import java.util.List;

/**
 * MainAbilitySlice
 *
 * @version 1.0
 * @since 2021-02-26
 */
public class MainAbilitySlice extends BaseSlice {
    private ListContainer listContainer;
    private BaseItemProvider provider;
    private List<ExampleItem> examples = new ArrayList<>();
    private BaseFraction currentFaction;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(getColor(ResourceTable.Color_colorPrimaryDark));
        initExamples();
        ((MainAbility) getAbility()).setCurrentAbilitySlice(this);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_lc);
        listContainer.setOrientation(Component.VERTICAL);
        listContainer.setItemProvider(provider = new HomeOptionsProvider(getContext(),examples));
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                if (currentFaction != null || currentFaction == examples.get(position).getCreateView()) { // 不能重复add相同的fraction
                    return;
                }

                currentFaction = examples.get(position).getCreateView();

                FractionScheduler fractionScheduler = ((FractionAbility) getAbility()).getFractionManager().startFractionScheduler();
                fractionScheduler.add(ResourceTable.Id_homeContainer, currentFaction)
                        .pushIntoStack(currentFaction.getClass().getSimpleName())
                        .submit();
                ((MainAbility) getAbility()).setCurrentFaction(currentFaction);
            }
        });

    }

    private void initExamples() {
        examples.add(new ExampleItem(ResourceTable.String_example_1_title, ResourceTable.String_example_1_subtitle, Example1Fraction::new));
        examples.add(new ExampleItem(ResourceTable.String_example_2_title, ResourceTable.String_example_2_subtitle, Example2Fraction::new));
        examples.add(new ExampleItem(ResourceTable.String_example_3_title, ResourceTable.String_example_3_subtitle, Example3Fraction::new));
        examples.add(new ExampleItem(ResourceTable.String_example_4_title, ResourceTable.String_example_4_subtitle, Example4Fraction::new));
        examples.add(new ExampleItem(ResourceTable.String_example_5_title, ResourceTable.String_example_5_subtitle, Example5Fraction::new));
        examples.add(new ExampleItem(ResourceTable.String_example_6_title, ResourceTable.String_example_6_subtitle, Example6Fraction::new));
        examples.add(new ExampleItem(ResourceTable.String_example_7_title, ResourceTable.String_example_7_subtitle, Example7Fraction::new));
    }

    public void setCurrentFaction(BaseFraction currentFaction) {
        this.currentFaction = currentFaction;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
