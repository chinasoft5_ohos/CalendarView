/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * ListExtension
 *
 * @version 1.0
 * @date 2021/2/22/022 11:10
 */
public class ListExtension<E> {
    private List<E> list;

    public ListExtension(List<E> list) {
        this.list = list;
    }

    /**
     * 获取ListExtension<E>
     *
     * @param collection List<E>
     * @param <E> 泛型E
     * @return ListExtension<E>
     */
    public static <E> ListExtension<E> getInstance(List<E> collection) {
        return new ListExtension<>(collection);
    }


    public List<E> getList() {
        return list;
    }

    /**
     * 判空
     *
     * @return ListExtension<E>
     */
    public ListExtension<E> orEmpty() {
        list = (list != null ? list : new ArrayList<E>());
        return this;
    }

    /**
     * 将对象添加到集合中
     *
     * @param e 泛型E
     * @return ListExtension<E>
     */
    public ListExtension<E> plus(E e) {
        list.add(e);
        return this;
    }


}
