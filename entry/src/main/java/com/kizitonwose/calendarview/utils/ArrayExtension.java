/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import com.kizitonwose.calendarview.NumberRange;

import java.util.Arrays;

/**
 * ArrayExtension
 *
 * @version 1.0
 * @date 2021/2/22/022 14:33
 */
public class ArrayExtension<T> {
    private T[] arrays;

    public ArrayExtension(T[] arrays) {
        this.arrays = arrays;
    }

    public static <T> ArrayExtension<T> getInstance(T[] arrays) {
        return new ArrayExtension<>(arrays);
    }

    /**
     * 切割数组
     *
     * @param range NumberRange
     * @return T[] 泛型类型数组
     */
    public T[] sliceArray(NumberRange range) {
        return Arrays.copyOfRange(arrays, range.getStart().intValue(), range.getEndInclusive().intValue());
    }
}
