/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

/**
 * Kotlin中String的方法
 *
 * @version 1.0
 * @date 2021/2/22 9:57
 */
public class StringExtension {
    private String string;

    private StringExtension(String string) {
        if (string == null) {
            string = "";
        }
        this.string = string;

    }

    public String getString() {
        return string;
    }

    /**
     * 获取单例StringExtension
     *
     * @param string
     * @return StringExtension 当前类对象
     */
    public static StringExtension getInstance(String string) {
        return new StringExtension(string);
    }

    /**
     * 空判定
     *
     * @param defaultValue
     * @return StringExtension 当前类对象
     */
    public StringExtension ifEmpty(String defaultValue) {
        string = (string == null ? "null" : string.length() == 0 ? defaultValue : string);
        return this;
    }

    /**
     * 是否是空白字符
     * Determines whether a character is whitespace according to the Unicode standard.
     * * Returns `true` if the character is whitespace.
     *
     * @return boolean
     */
    public boolean isBlank() {
        if (string.length() == 0){
            return true;
        }
        char[] chars = string.toCharArray();
        for (char aChar : chars) {
            if (!(Character.isWhitespace(aChar) || Character.isSpaceChar(aChar))) {
                return false;
            }
        }
        return true;
    }


}
