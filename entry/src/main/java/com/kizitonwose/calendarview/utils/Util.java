/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import com.kizitonwose.calendarview.ResourceTable;
import com.kizitonwose.calendarview.fraction.Example5Fraction;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

/**
 * Util
 *
 * @version 1.0
 * @date 2021/3/10 15:14
 */
public class Util {
    public static List<Example5Fraction.Flight> generateFlights() {
        List<Example5Fraction.Flight> list = new ArrayList<>();
        YearMonth currentMonth = YearMonth.now();
        LocalDate currentMonth17 = currentMonth.atDay(17);
        list.add(new Example5Fraction.Flight(currentMonth17.atTime(14, 0), new Example5Fraction.Airport("Lagos", "LOS"), new Example5Fraction.Airport("Abuja", "ABV"), ResourceTable.Color_brown_700));
        list.add(new Example5Fraction.Flight(currentMonth17.atTime(21, 30), new Example5Fraction.Airport("Enugu", "ENU"), new Example5Fraction.Airport("Owerri", "QOW"), ResourceTable.Color_blue_grey_700));

        LocalDate currentMonth22 = currentMonth.atDay(22);
        list.add(new Example5Fraction.Flight(currentMonth22.atTime(13, 20), new Example5Fraction.Airport("Ibadan", "IBA"), new Example5Fraction.Airport("Benin", "BNI"), ResourceTable.Color_blue_800));
        list.add(new Example5Fraction.Flight(currentMonth22.atTime(17, 40), new Example5Fraction.Airport("Sokoto", "SKO"), new Example5Fraction.Airport("Ilorin", "ILR"), ResourceTable.Color_red_800));


        list.add(new Example5Fraction.Flight(
                currentMonth.atDay(3).atTime(20, 0),
                new Example5Fraction.Airport("Makurdi", "MDI"),
                new Example5Fraction.Airport("Calabar", "CBQ"),
                ResourceTable.Color_teal_700)
        );

        list.add(
                new Example5Fraction.Flight(
                        currentMonth.atDay(12).atTime(18, 15),
                        new Example5Fraction.Airport("Kaduna", "KAD"),
                        new Example5Fraction.Airport("Jos", "JOS"),
                        ResourceTable.Color_cyan_700
                )
        );

        LocalDate nextMonth13 = currentMonth.plusMonths(1).atDay(13);
        list.add(new Example5Fraction.Flight(nextMonth13.atTime(7, 30), new Example5Fraction.Airport("Kano", "KAN"), new Example5Fraction.Airport("Akure", "AKR"), ResourceTable.Color_pink_700));
        list.add(new Example5Fraction.Flight(nextMonth13.atTime(10, 50), new Example5Fraction.Airport("Minna", "MXJ"), new Example5Fraction.Airport("Zaria", "ZAR"), ResourceTable.Color_green_700));

        list.add(
                new Example5Fraction.Flight(
                        currentMonth.minusMonths(1).atDay(9).atTime(20, 15),
                        new Example5Fraction.Airport("Asaba", "ABB"),
                        new Example5Fraction.Airport("Port Harcourt", "PHC"),
                        ResourceTable.Color_orange_800
                )
        );


        return list;
    }

    /**
     * 通过rgb值获取ShapeElement
     *
     * @param red 红色值
     * @param green 绿色值
     * @param blue 蓝色值
     * @return ShapeElement
     */
    public static ShapeElement getShapeElement(int red, int green, int blue) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(red, green, blue));
        return shapeElement;
    }

}
