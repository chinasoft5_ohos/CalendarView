/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * DisplayUtils
 *
 * @version 1.0
 * @date 2021/3/15/015 14:35
 */
public class DisplayUtils {

    /**
     * 获取屏幕的宽
     *
     * @param context
     * @return 屏幕的宽
     */
    public static float getWidth(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return point.getPointX();
    }

    /**
     * 获取屏幕的高
     *
     * @param context 上下文
     * @return 屏幕的高
     */
    public static float getHeight(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return point.getPointY();
    }
}
