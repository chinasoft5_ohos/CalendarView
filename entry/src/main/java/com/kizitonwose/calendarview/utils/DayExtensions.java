/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import com.kizitonwose.calendarview.NumberRange;

import java.time.DayOfWeek;
import java.time.temporal.WeekFields;
import java.util.*;

/**
 * DayExtensions
 *
 * @version 1.0
 * @since 2021-02-26
 */
public class DayExtensions {

    /**
     * 获取本地一周的数据
     *
     * @return DayOfWeek[] 数组
     */
    public static DayOfWeek[] daysOfWeekFromLocale() {
        DayOfWeek firstDayOfWeek = WeekFields.of(Locale.getDefault()).getFirstDayOfWeek();
        DayOfWeek[] daysOfWeek = DayOfWeek.values();
        // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
        // Only necessary if firstDayOfWeek != DayOfWeek.MONDAY which has ordinal 0.
        if (firstDayOfWeek != DayOfWeek.MONDAY) {
            DayOfWeek[] rhs = ArrayExtension.getInstance(daysOfWeek).sliceArray(new NumberRange(firstDayOfWeek.ordinal(), daysOfWeek.length));
            DayOfWeek[] lhs = ArrayExtension.getInstance(daysOfWeek).sliceArray(new NumberRange(0, firstDayOfWeek.ordinal()));
            List<DayOfWeek> list = new ArrayList<>(Arrays.asList(rhs));
            Collections.addAll(list, lhs);
            daysOfWeek = list.toArray(daysOfWeek);
        }
        return daysOfWeek;
    }
}
