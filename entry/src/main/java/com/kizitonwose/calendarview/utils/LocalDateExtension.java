/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain (a) copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kizitonwose.calendarview.utils;

import java.time.LocalDate;
import java.time.YearMonth;

/**
 * LocalDateExtension
 *
 * @version 1.0
 * @date 2021/2/23/023 16:10
 */
public class LocalDateExtension {
    private LocalDate localDate;

    private LocalDateExtension(LocalDate localDate) {
        this.localDate = localDate;
    }

    /**
     * 获取LocalDateExtension
     *
     * @param localDate
     * @return LocalDateExtension
     */
    public static LocalDateExtension getInstance(LocalDate localDate) {
        return new LocalDateExtension(localDate);
    }

    /**
     * 获取YearMonth
     *
     * @return YearMonth
     */
    public  YearMonth getYearMonth(){
        return YearMonth.of(localDate.getYear(), localDate.getMonth());
    }





}
